﻿using UnityEngine;

namespace Blazewing
{
    [CreateAssetMenu(fileName = "Float", menuName = "Variables/Float")]
    public class FloatVariable : BaseVariable
    {
        public float Value;
    }
}
