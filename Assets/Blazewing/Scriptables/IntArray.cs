﻿using UnityEngine;

namespace Blazewing
{
    [CreateAssetMenu(fileName = "IntArray", menuName = "Variables/IntArray")]
    public class IntArray : BaseVariable
    {
        public int[] Value;
    }
}
