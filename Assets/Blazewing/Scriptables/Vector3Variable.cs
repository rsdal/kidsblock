﻿using UnityEngine;

namespace Blazewing
{
    [CreateAssetMenu(fileName = "Int", menuName = "Variables/Vector3")]
    public class Vector3Variable : BaseVariable
    {
        public Vector3 Value;
    }
}
