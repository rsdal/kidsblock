﻿using System.Collections;
using System.Collections.Generic;
using Blazewing;
using UnityEngine;

[CreateAssetMenu(fileName = "ListGameobject", menuName = "Variables/ListGameobject")]
public class ListGameObjectVariable : BaseVariable
{
    public List<GameObject> Value;
}
