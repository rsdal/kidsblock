﻿using UnityEngine;

namespace Blazewing
{
    [CreateAssetMenu(fileName = "Int", menuName = "Variables/Int")]
    public class IntVariable : BaseVariable
    {
        public int Value;
    }
}
