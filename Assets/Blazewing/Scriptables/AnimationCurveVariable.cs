﻿using UnityEngine;

namespace Blazewing
{
    [CreateAssetMenu(fileName = "AnimationCurve", menuName = "Variables/Animation Curve")]
    public class AnimationCurveVariable : BaseVariable
    {
        public AnimationCurve Value;
    }
}
