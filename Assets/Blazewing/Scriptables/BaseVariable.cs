﻿using UnityEngine;

namespace Blazewing
{
    public class BaseVariable : ScriptableObject
    {
        [Header("Variable Description")]
        [TextArea(3, 10)]
        public string Description;
    }
}
