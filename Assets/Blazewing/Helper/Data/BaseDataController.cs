﻿using UnityEngine.SceneManagement;

namespace Blazewing
{
    public abstract class BaseDataController<T> : BaseController where T : struct
    {
        public static void Show(T data, string sceneName, LoadSceneMode mode)
        {
            DataController.Add(data);
            Show(sceneName, mode);
        }

        void Start()
        {
            var data = DataController.Get<T>();
            Initialize(data);
        }

        /// <summary>
        /// Initialize is called on start
        /// </summary>
        /// <param name="data"></param>
        public abstract void Initialize(T data);
    }
}
