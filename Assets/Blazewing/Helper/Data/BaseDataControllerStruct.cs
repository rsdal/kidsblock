﻿using UnityEngine.SceneManagement;

namespace Blazewing
{
    public struct BaseDataControllerStruct 
    {
        public string SceneName;
        public LoadSceneMode LoadMode;
    }
}
