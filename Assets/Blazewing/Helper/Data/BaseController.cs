﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Blazewing
{
    public abstract class BaseController : MonoBehaviour
    {
        public static void Show(string sceneName, LoadSceneMode mode)
        {
            if (!string.IsNullOrEmpty(sceneName))
                SceneManager.LoadSceneAsync(sceneName, mode);
        }

        public static void Hide(string sceneName)
        {
            if (!string.IsNullOrEmpty(sceneName))
                SceneManager.UnloadSceneAsync(sceneName);
        }
    }
}
