﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Blazewing
{
    /// <summary>
    /// Sound manager is a Singleton class that handles all the game musics and SFX.
    /// </summary>
    public class SoundManager : MonoBehaviour
    {
        // list of audio and music of the game
        public List<SoundPropertyScriptable> newMusics;
        public List<SoundPropertyScriptable> newSfxs;


        // audio mixer
        public AudioMixer mixer;


        // main music source
        public AudioSource musicsSource;


        // volume handlers
        public float audioVolume;
        public float musicVolume;


        // pitch controls
        public int comboPitch = 0; // the note
        public float[] comboPitches;
        public float transpose = -4;

        // aux clip sources to play many audio clips
        private ArrayList clipSources = new ArrayList();
        private ArrayList musicSources = new ArrayList();

        private ArrayList audioSoundProperties = new ArrayList();

        private void Awake()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            DontDestroyOnLoad(this.gameObject);
            this.clipSources = new ArrayList();
            this.audioSoundProperties = new ArrayList();
            this.musicSources = new ArrayList();

            // set player sound prefs
            if (PlayerPrefs.HasKey("META_MUSICCONFIG"))
            {
                float m = PlayerPrefs.GetFloat("META_MUSICCONFIG");
                float a = PlayerPrefs.GetFloat("META_AUDIOCONFIG");
                OnChangeMusicVolume(m);
                OnChangeAudioVolume(a);
            }
            else
            {
                OnChangeMusicVolume(1);
                OnChangeAudioVolume(1);
            }
        }

        public static float GetMusicVolume()
        {
            return PlayerPrefs.HasKey("META_MUSICCONFIG") ? PlayerPrefs.GetFloat("META_MUSICCONFIG") : 1;
        }

        public static float GetAudioVolume()
        {
            return PlayerPrefs.HasKey("META_AUDIOCONFIG") ? PlayerPrefs.GetFloat("META_AUDIOCONFIG") : 1;
        }

        void Update()
        {
            // set maximum combo pitch
            this.comboPitch = comboPitch > comboPitches.Length - 1 ? comboPitches.Length - 1 : comboPitch;

            // update sound sources
            if (this.clipSources != null && this.clipSources.Count > 0)
            {
                AudioSource a;
                for (int i = this.clipSources.Count - 1; i >= 0; i--)
                {
                    a = this.clipSources[i] as AudioSource;
                    a.volume = this.audioVolume;
                    if (!a.isPlaying)
                    {
                        this.clipSources.RemoveAt(i);
                        this.audioSoundProperties.RemoveAt(i);
                        Destroy(a);
                    }
                    else if (((SoundPropertyScriptable)this.audioSoundProperties[i]).ComboPitchInfluence)
                    {
                        a.pitch = Mathf.Pow(2, (comboPitches[comboPitch] + transpose) / 12);
                    }
                }
            }

            // update music sources
            if (this.musicSources != null && this.musicSources.Count > 0)
            {
                AudioSource a;
                for (int i = this.musicSources.Count - 1; i >= 0; i--)
                {
                    a = this.musicSources[i] as AudioSource;
                    a.volume = this.musicVolume;
                    if (!a.isPlaying)
                    {
                        this.musicSources.RemoveAt(i);
                        Destroy(a);
                    }
                }
            }

        }




        /// <summary>
        /// Change music volume
        /// </summary>
        /// <param name="value">Normalized value from 0 to 1</param>
        public void OnChangeMusicVolume(float value)
        {
            if (musicsSource != null)
                this.musicsSource.volume = value;
            this.musicVolume = value;

            PlayerPrefs.SetFloat("META_MUSICCONFIG", value);
            PlayerPrefs.Save();
        }



        /// <summary>
        /// Change SFX volume
        /// </summary>
        /// <param name="value">Normalized value from 0 to 1</param>
        public void OnChangeAudioVolume(float value)
        {
            this.audioVolume = value;
            PlayerPrefs.SetFloat("META_AUDIOCONFIG", value);
            PlayerPrefs.Save();
        }




        /// <summary>
        /// Call this function to play a music. Musics play in loop and one at time
        /// </summary>
        /// <param name="music">A string with the ID of the music named in the Prefab</param>
        /// <param name="additive">Boolean value that determine if stop the other music or not</param>
        public void PlayMusic(string music, bool additive = false)
        {
            SoundPropertyScriptable mus = GetSoundProperty(ref this.newMusics, music, true);
            if (mus == null)
            {
                Debug.LogError("SoundManager: Music " + music + " Not found");
                return;
            }

            if (mus.Disable) return;

            if (!additive)
            {
                StopAdditiveMusic();

                if (this.musicsSource.clip != mus.GetClip())
                {
                    this.musicsSource.Stop();
                    this.musicsSource.clip = mus.GetClip();
                    this.musicsSource.volume = this.musicVolume;
                    this.musicsSource.outputAudioMixerGroup = mus.Group;
                    this.musicsSource.Play();
                }
            }
            else
            {
                AudioSource s = gameObject.AddComponent<AudioSource>();
                s.volume = this.audioVolume;
                s.loop = true;
                s.playOnAwake = true;
                s.clip = mus.GetClip();
                s.outputAudioMixerGroup = mus.Group;
                s.Play();
                this.musicSources.Add(s);
            }
        }



        /// <summary>
        /// Stop everything - sounds and music
        /// </summary>
        public void StopAll()
        {
            AudioSource a;
            if (this.clipSources != null && this.clipSources.Count > 0)
            {
                for (int i = this.clipSources.Count - 1; i >= 0; i--)
                {
                    a = this.clipSources[i] as AudioSource;
                    a.Stop();
                    this.clipSources.RemoveAt(i);
                    this.audioSoundProperties.RemoveAt(i);
                    Destroy(a);
                }
            }

            StopAdditiveMusic();
            this.musicsSource.Stop();
        }





        public void StopAdditiveMusic()
        {
            AudioSource a;
            if (this.musicSources != null && this.musicSources.Count > 0)
            {
                for (int i = this.musicSources.Count - 1; i >= 0; i--)
                {
                    a = this.musicSources[i] as AudioSource;
                    a.Stop();
                    this.musicSources.RemoveAt(i);
                    Destroy(a);
                }
            }
        }





        /// <summary>
        /// Call this function to play some SFX. You can configure the sound with more option than PLayMusic
        /// </summary>
        /// <param name="sound">A string with the ID of the SFX named in the Prefab</param>
        /// <param name="repeat">If set to <c>true</c> play this SFX in loop</param>
        public void PlaySound(string sound, int pitch = -1, float delay = 0)
        {
            SoundPropertyScriptable sou = GetSoundProperty(ref this.newSfxs, sound, false);
            if (sou == null)
            {
                Debug.LogError("SoundManager: Sound " + sound + " Not found");
                return;
            }

            if (sou.Disable) return;

            AudioSource s = gameObject.AddComponent<AudioSource>();
            s.loop = sou.Loop;

            s.volume = this.audioVolume;
            s.clip = sou.GetClip();
            s.outputAudioMixerGroup = sou.Group;

            if (sou.RandomPitch)
                s.pitch = sou.GetPitch();

            //if (pitch >= 0)
            //    s.pitch = Mathf.Pow(2, (comboPitches[pitch] + transpose) / 12);

            s.PlayDelayed(delay);

            this.clipSources.Add(s);
            this.audioSoundProperties.Add(sou);
        }





        /// <summary>
        /// Stops a particular SFX.
        /// </summary>
        /// <param name="sound">A string with the ID of the music named in the Prefab</param>
        public void StopSound(string sound)
        {
            SoundPropertyScriptable sou = GetSoundProperty(ref newSfxs, sound, false);
            if (sou == null)
            {
                Debug.Log("Sound Not Found: " + sound);
                return;
            }

            if (sou.Random)
            {
                for (int i = 0; i < sou.Audio.Count; i++)
                {
                    StopSoundClip(sou.Audio[i]);
                }
            }
            else
            {
                StopSoundClip(sou.GetClip());
            }

        }

        private void StopSoundClip(AudioClip clip)
        {
            AudioClip a;
            AudioSource s;

            for (int i = this.clipSources.Count - 1; i >= 0; i--)
            {
                a = (this.clipSources[i] as AudioSource).clip;
                s = this.clipSources[i] as AudioSource;
                if (a == clip)
                {
                    s.loop = false;
                    s.Stop();
                    this.clipSources.RemoveAt(i);
                    this.audioSoundProperties.RemoveAt(i);
                    Destroy(s);
                }
            }
        }




        /// <summary>
        /// Search for a specific sound/music in the list and return that sound property
        /// </summary>
        /// <param name="target">array to search - can be Musics or SoundFXs</param>
        /// <param name="id">name of the sound</param>
        /// <returns>SoundProperty with audioclip and audiomixer properties</returns>
        internal SoundPropertyScriptable GetSoundProperty(ref List<SoundPropertyScriptable> target, string id, bool isMusic)
        {
            SoundPropertyScriptable c = null;

            if (target == null)
                target = new List<SoundPropertyScriptable>();

            for (int i = 0; i < target.Count; i++)
            {
                if (target[i].name == id)
                {
                    c = target[i];
                    break;
                }
            }

            if (c == null)
            {
                c = GetResourceSound(id, isMusic);

                if (c != null)
                    target.Add(c);
            }

            return c;
        }

        protected SoundPropertyScriptable GetResourceSound(string id, bool isMusic = false)
        {
            string path = string.Format("Audio/{0}/{1}", isMusic ? "Music" : "Sfx", id);

            SoundPropertyScriptable c = Resources.Load<SoundPropertyScriptable>(path);

            return c;
        }


        // @todo: dont know if is the best way to work with snapshots...
        /// <summary>
        /// Change Snapshot
        /// </summary>
        /// <param name="snapshot">Snapshot name to change</param>
        public void SnapshotTransition(string snapshot)
        {
            AudioMixerSnapshot snap = mixer.FindSnapshot(snapshot);
            if (snap == null)
            {
                Debug.LogError("SoundManager: mixer not found with this string - " + snapshot);
                return;
            }
            snap.TransitionTo(0);
        }

        private void OnAudioTrigger(SoundManagerStruct action)
        {
            switch (action.SoundAction)
            {
                case SoundManagerStruct.SoundActions.Music:
                    PlayMusic(action.Name);
                    break;
                case SoundManagerStruct.SoundActions.Sound:
                    PlaySound(action.Name);
                    break;
                case SoundManagerStruct.SoundActions.Snapshot:
                    SnapshotTransition(action.Name);
                    break;
                case SoundManagerStruct.SoundActions.ChangeMusicVolume:
                    OnChangeMusicVolume(action.Volume);
                    break;
                case SoundManagerStruct.SoundActions.ChangSoundVolume:
                    OnChangeAudioVolume(action.Volume);
                    break;
                default:
                    break;
            }
        }

        private void OnEnable()
        {
            DataEvent.Register<SoundManagerStruct>(OnAudioTrigger);
        }

        private void OnDisable()
        {
            DataEvent.Unregister<SoundManagerStruct>(OnAudioTrigger);
        }
    }
}
