﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Blazewing
{
    [CreateAssetMenu(fileName = "SoundProperty", menuName = "Sound/NewSound")]
    public class SoundPropertyScriptable : ScriptableObject
    {
        [Header("Audio clip list (more than one only get on random)")]
        public List<AudioClip> Audio;
        [Header("Must add a audiomixer group to this")]
        public AudioMixerGroup Group;
        [Header("Add a combo pitch influence")]
        public bool ComboPitchInfluence;
        [Header("Get a random audio")]
        public bool Random;
        public bool Loop;
        [Header("Diable Sound/Music to be played")]
        public bool Disable;
        [Header("Random Pitch")]
        public bool RandomPitch;
        public float Min, Max;
        [Header("Music/Sfx Description")]
        [TextArea(3, 10)]
        public string Description;

        public AudioClip GetClip()
        {
            if (Audio.Count > 0)
            {
                if (Random)
                    return Audio[UnityEngine.Random.Range(0, Audio.Count)];
                else
                    return Audio[0];
            }

            return null;
        }

        public float GetPitch()
        {
            return UnityEngine.Random.Range(Min, Max);
        }
    }
}
