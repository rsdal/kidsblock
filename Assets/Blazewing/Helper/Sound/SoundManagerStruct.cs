﻿namespace Blazewing
{
    public struct SoundManagerStruct 
    {
        public enum SoundActions
        {
            Music,
            Sound,
            Snapshot,
            ChangeMusicVolume,
            ChangSoundVolume
        }

        public SoundActions SoundAction;
        public string Name;
        public float Volume;

        public static void Trigger(SoundManagerStruct playerMatch)
        {
            DataEvent.Notify(playerMatch);
        }

        public static void PlayMusic(string name)
        {
            Trigger(new SoundManagerStruct() { SoundAction = SoundActions.Music, Name = name });
        }

        public static void PlaySound(string name)
        {
            Trigger(new SoundManagerStruct() { SoundAction = SoundActions.Sound, Name = name });
        }

        public static void ChangeMusicVolume(float volume)
        {
            Trigger(new SoundManagerStruct() { SoundAction = SoundActions.ChangeMusicVolume, Volume = volume});
        }

        public static void ChangeSoundVolume(float volume)
        {
            Trigger(new SoundManagerStruct() { SoundAction = SoundActions.ChangSoundVolume, Volume = volume });
        }

        public static void ChangeSnapshot(string name)
        {
            Trigger(new SoundManagerStruct() { SoundAction = SoundActions.Snapshot, Name = name });
        }
    }
}
