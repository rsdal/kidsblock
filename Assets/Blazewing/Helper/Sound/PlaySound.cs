﻿using UnityEngine;

namespace Blazewing
{
    public class PlaySound : MonoBehaviour
    {
        public bool PlayOnAwake;
        public string Name;
        public bool isMusic;

        private void Awake()
        {
            if (PlayOnAwake)
                Play();
        }

        public void Play()
        {
            if (string.IsNullOrEmpty(Name)) return;

            if (isMusic)
                SoundManagerStruct.PlayMusic(Name);
            else
                SoundManagerStruct.PlaySound(Name);
        }
    }
}
