﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KidsBlocks
{
    public class LoadSceneBehaviour : MonoBehaviour
    {
        public bool LoadOnStart;
        public string SceneName;

        IEnumerator Start()
        {
            if (LoadOnStart)
                LoadScene(SceneName);
            
            yield return null;
        }

        public void LoadScene(string scene)
        {
            if (string.IsNullOrEmpty(scene)) return;

            SceneManager.LoadSceneAsync(scene);
        }
    }
}