﻿using System.Collections.Generic;
using UnityEngine;

namespace KidsBlocks
{
    public static class Extensions 
    {
        public static void TryGetComponentAndAdd<T>(this GameObject gameObject, ref List<T> list)
        {
            if (gameObject.TryGetComponent(out T select))
            {
                list.Add(select);
            }
        }   
    }
}
