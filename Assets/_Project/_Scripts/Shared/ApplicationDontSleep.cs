﻿using UnityEngine;

namespace KidsBlocks
{
    public class ApplicationDontSleep : MonoBehaviour
    {
        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
    }
}
