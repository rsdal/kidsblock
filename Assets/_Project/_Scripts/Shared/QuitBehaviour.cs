﻿using UnityEngine;

namespace KidsBlocks
{
    public class QuitBehaviour : MonoBehaviour
    {
        public void Quit()
        {
            Application.Quit();
        }
    }
}
