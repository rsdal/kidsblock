﻿using UnityEngine;
using UnityEngine.UI;

namespace KidsBlocks
{
    public class SelectButtonInput : MonoBehaviour, IInput
    {
        [SerializeField]
        private Button button;

        private bool _selected;
    
        private void Awake()
        {
            button.onClick.AddListener(() =>
            {
                _selected = !_selected;
            });
        }

        public bool OnDeselect()
        {
            return !_selected;
        }

        public bool OnSelect()
        {
            return _selected;
        }

    
    }
}
