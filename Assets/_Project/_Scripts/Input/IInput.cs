﻿namespace KidsBlocks
{
    public interface IInput
    {
        bool OnDeselect();
        bool OnSelect();
    }
}