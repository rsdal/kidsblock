﻿using UnityEngine;

namespace KidsBlocks
{
    public class MouseInput : MonoBehaviour, IInput
    {
        public bool OnDeselect()
        {
            return Input.GetMouseButtonUp(0);
        }

        public bool OnSelect()
        {
            return Input.GetMouseButtonDown(0);
        }
    }
}