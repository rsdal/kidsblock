﻿using System.Collections.Generic;
using UnityEngine;

namespace KidsBlocks
{
    public class RandomGameobjectSpawn : MonoBehaviour, ISpawn
    {
        [SerializeField] private Transform transformRoot;
        [SerializeField] private ListGameObjectVariable gameObjectsPrefabs;

        public GameObject Spawn(Vector3 position)
        {
            var block = Instantiate(gameObjectsPrefabs.Value[Random.Range(0, gameObjectsPrefabs.Value.Count)], transformRoot);
            
            block.transform.position = new Vector3(position.x, position.y, position.z);

            return block;
        }
    }
}