﻿using System;
using System.Threading;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.Linq;
using Blazewing;
using UnityEngine.Serialization;

namespace KidsBlocks
{
    public class RandomSpawnOnPlane : BaseSpawner
    {
        [SerializeField] private Vector3Variable ofsset;
        [SerializeField] private ARPlaneManager _arPlaneManager;
        
        private ARPlane _plane;

        public override void Start()
        {
            base.Start();
            _arPlaneManager.planesChanged += OnArPlaneChanged;
        }

        private void OnArPlaneChanged(ARPlanesChangedEventArgs args)
        {
            if (_plane != null || args.added.Count <= 0) return;
            
            _plane = args.added.First();
            
            InstantiateBlocks();
        }

        public override void InstantiateBlocks()
        {
            for (var i = 0; i < howManyBlocks; i++)
                InstantiateBlock(i);
        }

        public override void InstantiateBlock(int index)
        {
            var position = _plane.transform.position;
            
            var newPos = new Vector3(position.x + ofsset.Value.x, (position.y + (index * ofsset.Value.y)) + ofsset.Value.y, position.z);
            
            var spawnObject = _randomSpawn.Spawn(newPos);
            
            var rigd = spawnObject.AddComponent<Rigidbody>();
            rigd.freezeRotation = true;   
        }

        private void OnDestroy()
        {
            _arPlaneManager.planesChanged -= OnArPlaneChanged;
        }
    }
}