﻿using UnityEngine;

namespace KidsBlocks
{
    public abstract class BaseSpawner : MonoBehaviour
    {
        [Header("How many block will spawn")]
        [SerializeField] protected int howManyBlocks;
    
        protected ISpawn _randomSpawn;
    
        public virtual void Start()
        {
            _randomSpawn = GetComponent<ISpawn>();
        }
    
        public abstract void InstantiateBlocks();
        public abstract void InstantiateBlock(int index);
    }
}