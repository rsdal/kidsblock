﻿using System;
using Blazewing;
using UnityEngine;
using UnityEngine.Serialization;

namespace KidsBlocks
{
    public class RandomSpawnerOnViewPort : BaseSpawner
    {
        [SerializeField]
        public Vector3Variable offset;
        
        public override void Start()
        {
            base.Start();

            InstantiateBlocks();
        }

        public override void InstantiateBlocks()
        {
            for (var i = 0; i < howManyBlocks; i++)
            {
                InstantiateBlock(i);
            }
        }

        public override void InstantiateBlock(int index)
        {
            //Left to right on width
            var x = (index * offset.Value.x) + offset.Value.x;
            //Center of the screen
            var y = 0.5f + offset.Value.y; 
            
            var pos = new Vector3(x, y, 1);

            if (Camera.main != null) 
                pos = Camera.main.ViewportToWorldPoint(pos);

            _randomSpawn.Spawn(pos);
        }
    }
}