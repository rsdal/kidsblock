﻿using UnityEngine;

namespace KidsBlocks
{
    public interface ISpawn
    {
        GameObject Spawn(Vector3 position);
    }
}