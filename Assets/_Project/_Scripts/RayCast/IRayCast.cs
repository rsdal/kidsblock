﻿using UnityEngine;

namespace KidsBlocks
{
    public interface IRayCast
    {
        Transform GetSelection();
        void CheckRaycast(Ray ray);
    }
}