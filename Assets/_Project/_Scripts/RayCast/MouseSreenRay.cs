﻿using UnityEngine;

namespace KidsBlocks
{
    public class MouseSreenRay : MonoBehaviour, IRay
    {
        public Ray CreateRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
    }
}