﻿using UnityEngine;

namespace KidsBlocks
{
    public class CenterOfSreenRay : MonoBehaviour, IRay
    {
        public Ray CreateRay()
        {
            return Camera.main.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0));
        }
    }
}