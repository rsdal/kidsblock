﻿using Blazewing;
using UnityEngine;

namespace KidsBlocks
{
    public class RayCastLayer : MonoBehaviour, IRayCast
    {
        public FloatVariable MaxRaycastDistance;
        public LayerMask TargetLayer;
        private Transform _selection;

        public Transform GetSelection()
        {
            return _selection;
        }

        public void CheckRaycast(Ray ray)
        {
            if (Physics.Raycast(ray, out var hit, MaxRaycastDistance.Value, TargetLayer))
            {
                _selection = hit.transform;
            }
            else
                _selection = null;
        }
    }
}