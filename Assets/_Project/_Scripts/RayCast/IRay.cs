﻿using UnityEngine;

namespace KidsBlocks
{
    public interface IRay
    {
        Ray CreateRay();
    }
}