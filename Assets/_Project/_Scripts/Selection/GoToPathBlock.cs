﻿using System;
using Blazewing;
using DG.Tweening;
using UnityEngine;

namespace KidsBlocks
{
    public class GoToPathBlock : MonoBehaviour, IDeselect
    {
        [SerializeField]
        private BlockPathSelect _blockPath;
        [SerializeField]
        private FloatVariable duration;
        [SerializeField]
        private AnimationCurveVariable curve;
        
        public void OnDeselect(Transform selection)
        {
            if (!_blockPath) return;

            selection.DOMove(_blockPath.GetCurrentSelected.position, duration.Value, false).SetEase(curve.Value);
        }
    }
}