﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Video;

namespace KidsBlocks
{
    public class SelectionManager : MonoBehaviour
    {
        private Transform _currentSelection;
        
        private IRay _ray;
        private IRayCast _rayCast;
        private IInput _input;
        
        private ISelect _select;
        private IDeselect _deselect;
        private IUpdate _update;

        private void Awake()
        { 
            _ray = GetComponent<IRay>();
            _rayCast = GetComponent<IRayCast>();
            _input = GetComponent<IInput>();
            
            _select = GetComponent<ISelect>();
            _deselect = GetComponent<IDeselect>();
            _update = GetComponent<IUpdate>();
        }

        private void Update()
        {
            HandleOnSelect();

            HandleOnSelectUpdate();

            HandleOnDeselect();
        }

        private void HandleOnSelect()
        {
            var onSelect = _input.OnSelect();

            if (!onSelect || _currentSelection != null) return;
            
            var ray = _ray.CreateRay();

            _rayCast.CheckRaycast(ray);

            _currentSelection = _rayCast.GetSelection();

            if (_currentSelection != null)
            {
                _select.OnSelect(_currentSelection);
            }
        }
        
        private void HandleOnSelectUpdate()
        {
            if (_currentSelection != null)
                _update.OnUpdate(_currentSelection);
        }
        
        private void HandleOnDeselect()
        {
            var onDeselect = _input.OnDeselect();

            if (!onDeselect) return;
            
            if (_currentSelection != null)
            {
                _deselect.OnDeselect(_currentSelection);
                
                _currentSelection = null;
            }
        }
    }
}