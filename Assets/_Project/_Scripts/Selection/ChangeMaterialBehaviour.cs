﻿using UnityEngine;

namespace KidsBlocks
{
    public class ChangeMaterialBehaviour : MonoBehaviour, IChangeMaterial
    {
        public void ChangeMaterial(GameObject gameObject, Material newMaterial)
        {
            var render = gameObject.GetComponent<MeshRenderer>();

            if(!render) return;
            
            var newMaterials = new Material[render.materials.Length];

            for (var i = 0; i < render.materials.Length; i++)
            {
                newMaterials[i] = newMaterial;
            }

            render.materials = newMaterials;
        }
    }
}