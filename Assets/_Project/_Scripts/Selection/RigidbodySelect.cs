﻿using UnityEngine;

namespace KidsBlocks
{
    public class RigidbodySelect : MonoBehaviour, ISelect, IDeselect
    {
        private Rigidbody _rigd;
    
        public void OnSelect(Transform selection)
        {
            _rigd = selection.GetComponent<Rigidbody>();

            if (IsRigdNull()) return;
            
            _rigd.isKinematic = true;
        }

        public void OnDeselect(Transform selection)
        {  
            if (IsRigdNull()) return;
            
            _rigd.isKinematic = false;
            _rigd.velocity = Vector3.zero;
        }
        
        private bool IsRigdNull()
        {
            if (!_rigd) return true;
            return false;
        }
    }
}
