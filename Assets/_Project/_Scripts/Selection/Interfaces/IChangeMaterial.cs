﻿using UnityEngine;

namespace KidsBlocks
{
    public interface IChangeMaterial
    {
        void ChangeMaterial(GameObject gameObject, Material newMaterial);
    }
}