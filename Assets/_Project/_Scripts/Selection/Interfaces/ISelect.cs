﻿using UnityEngine;

namespace KidsBlocks
{
    public interface ISelect
    {
        void OnSelect (Transform selection);
    }
}