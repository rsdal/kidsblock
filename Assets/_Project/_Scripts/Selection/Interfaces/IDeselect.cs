﻿using UnityEngine;

namespace KidsBlocks
{
    public interface IDeselect
    {
        void OnDeselect(Transform selection);
    }
}