﻿using UnityEngine;

namespace KidsBlocks
{
    public interface IUpdate
    {
        void OnUpdate(Transform selection);
    }
}