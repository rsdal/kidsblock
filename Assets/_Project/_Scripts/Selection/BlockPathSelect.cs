﻿using UnityEngine;

namespace KidsBlocks
{
    public class BlockPathSelect : ScreenWorldMovementSelection, ISelect, IDeselect, IUpdate
    {
        [SerializeField]
        private Material SelectedMaterial;

        private IChangeMaterial _changeMaterial;
        private Transform _currentSelectionBlock;

        public Transform GetCurrentSelected => _currentSelectionBlock;

        void Awake()
        {
            _changeMaterial = GetComponent<IChangeMaterial>();
        }
    
        public void OnSelect(Transform selection)
        {
            SetZCord(selection);
            SetOffSet(selection);
        
            _currentSelectionBlock = InstantiateSelectableBlock(selection).transform;
        }
    
        private GameObject InstantiateSelectableBlock(Transform selection)
        {
            var instantiatedBlock = Instantiate(selection.gameObject, selection.position, Quaternion.identity);

            if (_changeMaterial != null) 
                _changeMaterial.ChangeMaterial(instantiatedBlock, SelectedMaterial);

            return instantiatedBlock;
        }

        public void OnDeselect(Transform selection)
        {
            Destroy(_currentSelectionBlock.gameObject);
        }

        public void OnUpdate(Transform selection)
        {
            _currentSelectionBlock.transform.position = GetPosition();
        }

        protected override Vector3 GetReferencePos()
        {
            return Input.mousePosition;
        }
    }
}
