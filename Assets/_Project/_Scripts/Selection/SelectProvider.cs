﻿using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace KidsBlocks
{
    public class SelectProvider : MonoBehaviour, ISelect, IDeselect, IUpdate
    {
        [SerializeField]
        private List<GameObject> selections;

        private List<ISelect> _selections = new List<ISelect>();
        private List<IDeselect> _deselects = new List<IDeselect>();
        private List<IUpdate> _selectUpdates = new List<IUpdate>();

        private void Awake()
        {
            CacheLists();
        }

        private void CacheLists()
        {
            foreach (var item in selections)
            {
                item.TryGetComponentAndAdd(ref _selections);
                item.TryGetComponentAndAdd(ref _deselects);
                item.TryGetComponentAndAdd(ref _selectUpdates);
            }
        }

        public void OnSelect(Transform selection)
        {
            _selections.ForEach(f=> f.OnSelect(selection));
        }

        public void OnDeselect(Transform selection)
        {
            _deselects.ForEach(f=> f.OnDeselect(selection));
        }

        public void OnUpdate(Transform selection)
        {
            _selectUpdates.ForEach(f=> f.OnUpdate(selection));
        }
    }
}
