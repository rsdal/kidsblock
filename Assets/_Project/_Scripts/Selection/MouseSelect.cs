﻿using UnityEngine;

namespace KidsBlocks
{
    public class MouseSelect : ScreenWorldMovementSelection, ISelect, IUpdate
    {
        public void OnSelect(Transform selection)
        {
            SetZCord(selection);
            SetOffSet(selection);
        }

        public void OnUpdate(Transform selection)
        {
            if (selection == null) return;
            
            selection.transform.position = GetPosition();
        }

        protected override Vector3 GetReferencePos()
        {
            return Input.mousePosition;
        }
    }
}
