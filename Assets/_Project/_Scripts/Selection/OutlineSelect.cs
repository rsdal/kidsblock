﻿using System;
using UnityEngine;
using System.Linq;

namespace KidsBlocks
{
    [Serializable]
    public class OutlineSelect : MonoBehaviour, ISelect, IDeselect
    {
        private Outline _outline;

        public void OnSelect(Transform selection)
        {
            _outline = selection.gameObject.GetComponent<Outline>();

            if (IsOutlineNull()) return;

            ChangeOutlineWidth(10);
        }

        private void ChangeOutlineWidth(float width)
        {
            _outline.OutlineWidth = width;
        }

        public void OnDeselect(Transform selection)
        {
            if (IsOutlineNull()) return;
            
            _outline.OutlineWidth = 0;
        }

        private bool IsOutlineNull()
        {
            if (!_outline) return true;
            return false;
        }
    }
}