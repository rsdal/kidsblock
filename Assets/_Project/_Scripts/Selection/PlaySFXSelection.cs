﻿using Blazewing;
using UnityEngine;

namespace KidsBlocks.Selection
{
    public class PlaySFXSelection : MonoBehaviour, ISelect, IDeselect
    {
        public string AudioToPlay;
        
        public void OnSelect(Transform selection)
        {
            SoundManagerStruct.PlaySound(AudioToPlay);
        }

        public void OnDeselect(Transform selection)
        {
            SoundManagerStruct.PlaySound(AudioToPlay);
        }
    }
}
