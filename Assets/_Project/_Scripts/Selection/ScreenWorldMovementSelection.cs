﻿using UnityEngine;

namespace KidsBlocks
{
    public abstract class ScreenWorldMovementSelection : MonoBehaviour
    {
        private Vector3 _offset;
        private float _zCord;

        public Vector3 GetOffSet => _offset;
    
        protected void SetZCord(Transform selection)
        {
            _zCord = Camera.main.WorldToScreenPoint(selection.position).z;
        }
    
        protected void SetOffSet(Transform selection)
        {
            _offset = selection.transform.position - GetWorldPos(GetReferencePos());
        }
    
        protected Vector3 GetWorldPos(Vector3 position)
        {
            position.z = _zCord;
            return Camera.main.ScreenToWorldPoint(position);
        }

        protected Vector3 GetPosition()
        {
            return GetWorldPos(GetReferencePos()) + GetOffSet;
        }
    
        protected abstract Vector3 GetReferencePos();
    }
}
